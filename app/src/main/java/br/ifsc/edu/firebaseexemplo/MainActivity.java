package br.ifsc.edu.firebaseexemplo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    EditText editTextLogin, editTextSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextLogin = findViewById(R.id.editTextLogin);
        editTextSenha = findViewById(R.id.editTextSenha);

        mAuth = FirebaseAuth.getInstance();

        //Create new account
//        mAuth.createUserWithEmailAndPassword("lydia@ifsc.edu.br", "123mudar");
//        mAuth.signInWithEmailAndPassword("lydia@ifsc.edu.br", "123mudar").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//            @Override
//            public void onComplete(@NonNull Task<AuthResult> task) {
//                if (task.isSuccessful()) {
//                    Toast.makeText(MainActivity.this, mAuth.getCurrentUser().getEmail(), Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(MainActivity.this, PrincipalActivity.class);
//                    startActivity(intent);
//                } else {
//                    Toast.makeText(MainActivity.this, "Falha no login", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//        FirebaseUser firebaseUser = mAuth.getCurrentUser();
//
//        if (firebaseUser != null) {
//            Log.d("FirebaseUserExemplo", "Usuário Logado " + firebaseUser.getEmail());
//        } else {
//            Log.d("FirebaseUserExemplo", "falha na autenticação");
//        }
    }

    public void login(View view) {
        String login = editTextLogin.getText().toString();
        String senha = editTextSenha.getText().toString();
        mAuth.signOut();

        if (!login.trim().equals("")) {
            mAuth.signInWithEmailAndPassword(login, senha)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(MainActivity.this, "Sucesso", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Erro", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            if (mAuth.getCurrentUser() != null) {
                Intent intent = new Intent(this, PrincipalActivity.class);
                startActivity(intent);
            }
        }
    }

    public void activityCadastrar(View view) {
        Intent intent = new Intent(this, CadastrarUsuarioActivity.class);
        startActivity(intent);

    }

    public void recuperarSenha(View view) {
        if (!editTextLogin.getText().toString().trim().equals("")) {
            mAuth.sendPasswordResetEmail(editTextLogin.getText().toString());
        }
    }
}
