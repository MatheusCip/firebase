
package br.ifsc.edu.firebaseexemplo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class CadastrarUsuarioActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    EditText editTextLogin, editTextSenha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_usuario);
        mAuth = FirebaseAuth.getInstance();

        editTextLogin = findViewById(R.id.editTextLoginCad);
        editTextSenha = findViewById(R.id.editTextSenhaCad);

    }

    public void cadastrarNovoUsuario(View view) {
        if (!editTextLogin.getText().toString().trim().equals("")) {
            mAuth.createUserWithEmailAndPassword(
                    editTextLogin.getText().toString(),
                    editTextSenha.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(CadastrarUsuarioActivity.this, "Usuário Cadastrado", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(CadastrarUsuarioActivity.this, "Falha no cadastro do Usuário", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }
}
